import '../01-useState/counter.css';
import { useState } from 'react';
export const useCounter=(inicialState=10)=>{

    const [state,setCounter]=useState(inicialState)

    const increment=()=>{
        
        setCounter(state+1)
    }
    const decrement=()=>{
        setCounter(state-1)
    }

    const reset=()=>{
        setCounter(inicialState)
    }
    
    return {
        state,
        increment,
        decrement,
        reset,
    }
}