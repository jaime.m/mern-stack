import React, { useLayoutEffect, useRef } from 'react'
import './layout.css'
import { useFetch } from '../hooks/useFetch'
import { useCounter } from '../hooks/useCounter'

export const Layout = () => {
    const {state,increment}=useCounter(1);
    
    const pTag = useRef()
    const {data}=useFetch(`https://breakingbadapi.com/api/quotes/${state}`);
console.log('soy yo ',data,state);

    const {quote}= !!data && data[0]
    useLayoutEffect(() => {
        console.log(pTag.current.getBoundingClientRect());
    }, [quote])


    
    return (
        <>
            <h1>Layout</h1>  
            <hr/>
           {
            <blockquote
            
            className='blockquote text-right'>
                <p 
                ref={pTag}
                className='mb-0'>{quote}</p>
            
            </blockquote>
           
           }
           <button className='btn btn-primary' onClick={increment}>Siguiente quote</button> 
        </>
    )
}
