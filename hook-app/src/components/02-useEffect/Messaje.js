import React, { useEffect, useState } from 'react'

export const Messaje = () => {

    const [Coords, setCoord] = useState({x:0,y:0})
    const {x,y}=Coords
    useEffect(() => {
        console.log('componente montado');

        const mouseMove=(e)=>{
            const cords={
                x:e.x,y:e.y
            }
            setCoord(cords)
            console.log(cords);
        }
        window.addEventListener('mousemove', mouseMove)
        
        return () => {
            console.log('componente desmontado');
            window.removeEventListener('mousemove',mouseMove)
        }
    }, [])


    return (
        <div>
            <h1>Claro si no hay nada muy duro jaja</h1>
            <p>X:{x} Y:{y}</p>
        </div>
    )
}
