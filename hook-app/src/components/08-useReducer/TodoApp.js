import React, { useReducer, useEffect } from 'react'
import {useForm} from '../../components/hooks/useForm'
import './styles.css'
import { todoReducer } from './todoReducer'
 
export const TodoApp = () => {

    const [{description},handleInputChange,reset]=useForm({
        description:'',

    })
    const init=()=>{
        return JSON.parse( localStorage.getItem('todos'))||[];
    /*     return [{
            id:new Date().getTime(),
            desc:'aprenderReact',
            done:false,
        }] */
    }
    
    console.log(description);
    const [todos,dispatch] = useReducer(todoReducer, [], init)
    
    useEffect(() => {
        localStorage.setItem('todos',JSON.stringify(todos))
    }, [todos]);
    const handleSubmit=(e)=>{
        e.preventDefault();

        if(description.trim().length<1)
            return

        const newTodo={
            id:new Date().getTime(),
            desc:description,
            done:false,            
        }

        const action={
            type:'add',
            payload:newTodo,
        }
        console.log('nuevaTarea');
        dispatch(action);
        reset();
    }
    return (
        <div>
            <h1>TodoApp ({todos.length})</h1>
            <hr/>

<div className='row'>
<div className='col-7'>

<ul className='list-group list-group-flush'>
                {
            todos.map((item,i)=>{
            return<li className='list-group-item' key={item.id}>
                  <p className='text-center'>  {i+1}.{item.desc}
                    </p>
                 <button className='btn btn-danger'> delete</button>
                </li>
            })
                }
            </ul>
</div>

<div className='col-5'> 
<h4> Agregar TODO</h4>
<hr/>
<form onSubmit={handleSubmit}>
    <input
    type='text'
    name='description'
    placeholder='Aprender...'
    autoComplete='off'
    value={description}
    onChange={handleInputChange}
    />
    <button type='submit' className='btn btn-outline-primary mt-1 btn-block'> Agregar</button>
</form>

</div>
</div>
        </div>
    )
}
