const initialState=[{
    id:1,
    todo:'comprar cositas',
    done:false,
}];

const todoReducer =(state=initialState,action)=>{
    if(action?.type==='agregar'){
        return [...state,action.payload]
    }
    return state;
}

let todos=todoReducer();

const newTodo=[{
    id:2,
    todo:'comprar cositas 2',
    done:false,
}];

const agregarTodoAction={
    type:'agregar',
    payload:newTodo,
}

todos= todoReducer(todos,agregarTodoAction)

console.log(todos);