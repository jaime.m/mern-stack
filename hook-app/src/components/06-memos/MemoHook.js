import React, { useState, useMemo } from 'react'
import '../02-useEffect/effects.css'
import { useCounter } from '../hooks/useCounter'


export const MemoHook = () => {
    const {state,increment}=useCounter(100);

    const procesoPesado=(ite)=>{
        for (let index = 0; index < ite; index++) {
            console.log('vamos!');
        }
    }
    const memoProcesoPesado=useMemo(() => procesoPesado(state), {state});

    const [show, setShow] = useState(true)
    return (
        <div>
            <h1>MemoHook</h1>
            <h1>Counter : {state}  </h1>
            <hr/>
            <p>{memoProcesoPesado}</p>
            <button className='btn btn-primary' onClick={increment}>
                +1
            </button>

            <button className='btn btn-primary' onClick={()=>{setShow(!show)}}>show/hide {JSON.stringify(show)}</button>
        </div>
    )
}
