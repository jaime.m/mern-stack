import { useState, useEffect } from "react"
import { getGif } from "../helpers/getGifs"

export const useFetchGifs=(category,api)=>{

    const [state,setState]=useState({
        data:[],
        loading:true
    })

    useEffect(()=>{
        getGif(category,api).then(imgs=>{
            setState({
                data:imgs,
                loading:false
            })
        })
    },[category])

/*     setTimeout(() => {
        setState({
            data:[1,2,3],
            loading:false
        })
    }, 3000); */

    return state;

}