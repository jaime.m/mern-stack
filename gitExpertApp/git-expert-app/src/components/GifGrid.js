
import  React, { useState, useEffect } from "react";
import { GifGridItem } from "./GifGridItem";
import {getGif} from '../helpers/getGifs'
import { useFetchGifs } from "../hooks/useFetchGifs";
import PropTypes from 'prop-types'; 
const api ='1oe8eqkihnR6UjEVSWEFMeXUYCgXFAhT'

export const GifGrid = ({category})  => {
    
   const {data: images,loading}= useFetchGifs(category,api);
   
/*     const [images,setImages] = useState([]);

    useEffect(()=>{
        getGif(category,api).then(setImages)
    },[category]) */
 /*    const getGif=async ()=>{

        const url= (`http://api.giphy.com/v1/gifs/search?q=${encodeURI(category)}&limit=5&api_key=${api}`)
        
        const resp= await fetch(url);
        const {data}= await resp.json();
        const gifs =data.map(img=>{
            return {
            img:    img.id,
            title:img.title,
            url:img.images?.downsized_medium.url
            };
        });
        //console.log(gifs);
        setImages(gifs);
    }
     */
   //getGif();
   console.log(images);
   
    return ( 
        <>
    <h3>{category}</h3>
     {loading&&  <p className="animate__animated animate__flash">loading </p>} 
     <div className ="card-grid  animate__animated animate__fadeIn" >
    <ol>
        {        
        images.map((img)=>{
            console.log(img.id);
            
            return <GifGridItem key= {img.id}  {...img}/>
        })
        
        }

    </ol>
    </div> 
    </>
    )
}



GifGrid.propTypes={
    category:PropTypes.string.isRequired
}