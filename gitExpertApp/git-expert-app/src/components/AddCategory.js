
import  React, { useState } from "react";
import PropTypes from 'prop-types'

export const AddCategory = ({setCategories}) => {
 const [inputValue,setInputValue]=useState('')

 const handleInputChange =({target})=>{
    setInputValue(target.value)
    console.log('handle input change');
    
 }
 const handleSubmit =(e)=>{

    e.preventDefault();
    if(inputValue.trim().length>1){
        setCategories(cats=>[inputValue,...cats])
        setInputValue('');
    }
 }
    return ( 
    <form onSubmit={handleSubmit}>
      <p>{inputValue}</p> 
      <h2></h2>
       <input
        type="text"
        value={inputValue}
        onChange={handleInputChange}
       />
       </form>
    )
}

AddCategory.propTypes={
    setCategories:PropTypes.func.isRequired
}
