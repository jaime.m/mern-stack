
import  React, { useState } from "react";
import { AddCategory } from "./components/AddCategory";
import { GifGrid } from "./components/GifGrid";

export const GifExpert = ({defaultCategories=[]}) => {

   // const categories =['one punch', 'samurai X','dgb']

 //  const [categories,setCategories]=useState(['One punch'])
   const [categories,setCategories]=useState(defaultCategories)

/*    const handleAdd=()=>{
   // setCategories( [...categories,'hunterxhunter']);
    setCategories(cats=>[...categories,'hunterxhunter'])
   } */

    return ( 
    <div>
        <h2>GifExpert</h2>
        <AddCategory setCategories={setCategories}/>
        <hr/>
        <ol>
            {
                categories.map((cat,idx)=>
                     <GifGrid key={cat} category ={cat}/> 
                )
            }
        </ol>
{/*         <button onClick={handleAdd}> ADD</button>
 */}       
    </div>
    )
}