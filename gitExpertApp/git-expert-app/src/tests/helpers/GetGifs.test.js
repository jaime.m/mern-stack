const { shallow } = require("enzyme")
import React from 'react';
import {getGif} from '../../helpers/getGifs';

describe('Pruebas con getGifs',()=>{
    const api ='1oe8eqkihnR6UjEVSWEFMeXUYCgXFAhT'

    test('debe traer 5 elementos',async ()=>{

        const gits = await getGif('one punch',api);

        expect(gits.length).toBe(5);
    })

    
    test('debe traer 5 elementos',async ()=>{

        const gits = await getGif('',api);

        expect(gits.length).toBe(0);
    })

 
})