const { shallow } = require("enzyme")
import React from 'react';
import {GifExpert} from '../GifExpertApp';

describe('Pruebas en <GifExpert/>',()=>{

  
  test('debe mostrar el componente correctamente',()=>{
    const wrapper =shallow(<GifExpert />)
        expect(wrapper).toMatchSnapshot();
    })

    test('debe mostrar Una lista de categorias',()=>{
      const categories=['one punch','dragon ball']
      const wrapper =shallow(<GifExpert defaultCategories={categories}/>)
          
      expect(wrapper).toMatchSnapshot();
      expect(wrapper.find('GifGrid').length).toBe(categories.length);
      })
})