const { shallow } = require("enzyme")
import React from 'react';
import {GifGridItem} from '../../components/GifGridItem';

describe('Pruebas en <GrifGridItem/>',()=>{

    const title='titulo';
    const url='soy una url'
    const wrapper =shallow(<GifGridItem title={title} url={url}/>)

    test('debe mostrar el componente correctamente',()=>{
        expect(wrapper).toMatchSnapshot();
    })

    
    test('debe de tener un parrafo con el titulo',()=>{       
        const p= wrapper.find('p')
        expect(p.text().trim()).toBe('titulo');
    })

    test('debe de tener la imagen igual al url de los props',()=>{       
        const p= wrapper.find('img')
        const sr= p.props('src')
        const alt= p.props('alt')

        expect(sr.src).toBe(url);
        expect(alt.alt).toBe(title);
    })

    test('Div debe tener la clase animated_fade in',()=>{       
        const p= wrapper.find('div')    
        const sr= p.props('className')
        console.log(sr.className);
        
        expect(sr.className.includes('animate__fadeIn')).toBe(true);
    })
})