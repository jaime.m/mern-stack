const { shallow } = require("enzyme")
import React from 'react';
import {GifGrid} from '../../components/GifGrid';
import { useFetchGifs } from '../../hooks/useFetchGifs';
jest.mock('../../hooks/useFetchGifs')
describe('Pruebas en <GifGrid/>',()=>{

    const title='titulo';
    const url='soy una url'
    

    test('debe mostrar el componente correctamente',()=>{
        useFetchGifs.mockReturnValue({
            data:[],
            loading:true,
        })
      const  wrapper =shallow(<GifGrid key={title} category={url}/>)

        expect(wrapper).toMatchSnapshot();
    })

    test('debe mostrar items cuando se cargan imágenes useFetchGif',()=>{
        const gifs=[{
            id:'abc',
            url:'https://sfdsdf.jpg',
            title:'gifzaso',
        },]
        useFetchGifs.mockReturnValue({
            data:gifs,
            loading:true,
        })
       const  wrapper =shallow(<GifGrid key={title} category={url}/>)
       expect(wrapper.find('p').exists()).toBe(true)
       expect(wrapper.find('GifGridItem').length).toBe(gifs.length)
      // expect(wrapper).toMatchSnapshot();
    })

})