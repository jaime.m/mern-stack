const { shallow } = require("enzyme")
import React from 'react';
import { AddCategory } from '../../components/AddCategory';

describe('Pruebas en <AddCategory/>',()=>{
    const setCategories=jest.fn();
    let wrapper = shallow(<AddCategory setCategories={setCategories}/>);

    beforeEach(()=>{
        wrapper = shallow(<AddCategory setCategories={setCategories}/>);
    })
    
    test('debe mostrare correctamente',async ()=>{
        expect(wrapper).toMatchSnapshot();
    })

    test('Debe de cambiarse la caja de texto',async ()=>{
        const input = wrapper.find('input')
        const value='hm'
        
        input.simulate('change',{target:{value}});  
        const par = wrapper.find('p').text().trim();

        console.log('loguedand ', par);
        expect(par).toBe(value);
      
    })

    test('No debe postear la información on submit si no se cumplen las condiciones de tamaño',async ()=>{
        
        wrapper.find('form').simulate('submit',{preventDefault(){}});

        expect(setCategories).not.toHaveBeenCalled()
    })

    test('Se debe verificar que la funcion se llamó y limpiar la caja de texto',async ()=>{
        //simular el input change
        const input = wrapper.find('input')
        const value='hm'
        input.simulate('change',{target:{value}});  

        //Simular el set submit
        wrapper.find('form').simulate('submit',{preventDefault(){}});
        //Expect de set categories llamado
        expect(setCategories).toHaveBeenCalledTimes(1)
        //input vacío
        const par = wrapper.find('input').text().trim();
        expect(par).toBe('');

    })
    
})