import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { GifExpert } from './GifExpertApp';

ReactDOM.render(
    <GifExpert />,
  document.getElementById('root')
);