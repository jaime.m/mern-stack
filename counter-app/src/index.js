import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import CounterApp from './CounterApp';
import PrimeraApp from './PrimeraApp';


const divRoot = document.querySelector('#root')

ReactDOM.render(<CounterApp value= {12}/>,divRoot);
//ReactDOM.render(<PrimeraApp sald= 'Hola mundo'/>,divRoot);
