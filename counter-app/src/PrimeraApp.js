//Functional components 
import React from 'react'
import PropTypes from 'prop-types'

const PrimeraApp =({sald, subtitulo})=>{


    return  (
        <>
        <h1>{sald}</h1>
        {/* <pre>{JSON.stringify(saludo,)}</pre> */}
        <p>{subtitulo}</p>
        </>
        )
}

PrimeraApp.propTypes={
    sald:PropTypes.string.isRequired
}
PrimeraApp.defaultProps={
    subtitulo:'soy un subtitulo'
}

export default PrimeraApp;