//Functional components 
import React, {useState} from 'react'
import PropTypes from 'prop-types'

const CounterApp =({value=10})=>{

    const [counter,setCounter] = useState(value)

    //HandleAdd
    const handleAdd=(e)=>
    {
        console.log(counter);
        
       setCounter(counter+1)
       console.log(counter);

        //setCounter((c)=>c+1)
    }
    const handleReset=(e)=>
    {
       setCounter(value)
        //setCounter((c)=>c+1)
    }
    const handleSubstract=(e)=>
    {
       setCounter(counter-1)
        //setCounter((c)=>c+1)
    }
        
    return  (
        <>
        <h1>CounterApp</h1>
        <h2>{counter}</h2>
        <button onClick={handleAdd} >+1</button>
        <button onClick={handleReset}> Reset</button>
        <button onClick={handleSubstract} >-1</button>
        </>
        )
}

CounterApp.propTypes={
    value:PropTypes.number.isRequired
}
CounterApp.defaultProps={
    value:0
}

export default CounterApp;