const { render } = require("@testing-library/react");
const { default: PrimeraApp } = require("../PrimeraApp");
import React from 'react'
import '@testing-library/jest-dom/extend-expect';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import '@testing-library/jest-dom'
configure({adapter: new Adapter()});
import {createSerializer} from 'enzyme-to-json';
expect.addSnapshotSerializer(createSerializer({mode:"deep"}))

describe('Pruebas en el archivo <PrimeraApp/>',()=>{

/*     test('debe de mostrar el mensaje "Hola Mundo" ',()=>{
        //Inicialización
        const saludo='Hola Mundo';
        //Estimulo
        const {getByText}= render(<PrimeraApp sald={saludo}/>)
        expect(getByText(saludo)).toBeInTheDocument();

    }) */

    //Enzime tests

    test('Should show <PrimeraApp/> correctly',()=>{
        const sald='hola'
        const wrapper=shallow(<PrimeraApp sald={sald}/>);
        expect(wrapper).toMatchSnapshot();
    })

    test('Should show the subtitle sended by props',()=>{
        const sald='hola'
        const subtitulo='subtitle'
        const wrapper=shallow(
        <PrimeraApp sald={sald}
        subtitulo={subtitulo}
        />
        );

        const textoParrafo= wrapper.find('p').text()

        expect(textoParrafo).toBe(subtitulo);
    })

})

