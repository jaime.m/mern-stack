const { render } = require("@testing-library/react");
const { default: CounterApp } = require("../CounterApp");
import React from 'react'
import '@testing-library/jest-dom/extend-expect';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import '@testing-library/jest-dom'
configure({adapter: new Adapter()});
import {createSerializer} from 'enzyme-to-json';
expect.addSnapshotSerializer(createSerializer({mode:"deep"}))

describe('Pruebas en el archivo <CounterApp/>',()=>{
    //Enzime tests
    let wrapper=shallow(<CounterApp value={10} />);

    beforeEach(()=>{
        wrapper=shallow(<CounterApp value={10} />);
    })
    test('Should show <CounterApp/> correctly',()=>{
        const sald=12
         wrapper=shallow(<CounterApp value={sald}/>);
        expect(wrapper).toMatchSnapshot();
    })

    test('Should show the value of 100',()=>{
        const sald=100
         wrapper=shallow(
        <CounterApp value={sald}/>
        );

        const textoParrafo= wrapper.find('h2').text().trim()

        expect(textoParrafo).toBe('100');
    })


    test('Should increase the value with the button of +1',()=>{
        
        
        wrapper.find('button').at(0).simulate('click');

        const textoParrafo= wrapper.find('h2').text().trim();

        expect(textoParrafo).toBe('11');
    })

    test('Should decrease the value with the button of -1',()=>{

        wrapper.find('button').at(2).simulate('click');

        const textoParrafo= wrapper.find('h2').text().trim()

        expect(textoParrafo).toBe('9');
    })

    test('Should put in the original value with the button of restart',()=>{
        const sald=100
        wrapper=shallow(
       <CounterApp value={sald}/>
       );
       wrapper.find('button').at(2).simulate('click');

        wrapper.find('button').at(1).simulate('click');

        const textoParrafo= wrapper.find('h2').text().trim()

        expect(textoParrafo).toBe('100');
    })

})

