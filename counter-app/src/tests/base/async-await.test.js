const { getImagen } = require("../../base/11-async-await");


describe('Pruebas con async await',()=>{

    test(' Debe retornar el url de la imagen ',async ()=>{

        const url= await getImagen();
        expect(url.includes('http')).toBe(true);

    })

    /*test(' Debe retornar no existe si no hay url ',async ()=>{

        //Inicialización
        const url= await getImagen();
        expect(url).toBe('No existe')
    })*/

})
