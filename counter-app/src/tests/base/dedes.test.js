
const { retornaArreglo } = require("../../base/07-deses-arr");

describe('Pruebas en el archivo funciones',()=>{

    test(' getUser debe retornar un string y un numero ',()=>{

        //Inicialización
        const [letras, numeros]= retornaArreglo();
    //const arreglo =retornaArreglo();

        expect(typeof(letras)).toEqual('string')
        expect(typeof(numeros)).toEqual('number')

    })

})
