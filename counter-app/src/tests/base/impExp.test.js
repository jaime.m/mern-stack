const { getHeroeById ,getHeroesByOwner } = require("../../base/08-imp-exp");
import {heroes} from '../../data/heroes'
describe('Pruebas en el archivo imp exp',()=>{

    test(' getHeroeById debe retornar un Heroe por el id ',()=>{

        //Inicialización
        const id =1;
        const hero= getHeroeById(1);
        const heroesDAta=heroes.find( (heroe) => heroe.id === id )
    //const arreglo =retornaArreglo();

        expect(hero).toEqual(heroesDAta)        

    })

    test(' getHeroeById debe retornar undefined si el id no existe ',()=>{

        //Inicialización
        const hero= getHeroeById(10);
    //const arreglo =retornaArreglo();

        expect(hero).toBe(undefined)        
        
    })

    test(' getHeroesByOwner debe retornar los heroes de DC ',()=>{

        const owner="DC"
        const heroes= getHeroesByOwner(owner);
        const heroesDc = [
            {
                id: 1,
                name: 'Batman',
                owner: 'DC'
            },
            {
                id: 3,
                name: 'Superman',
                owner: 'DC'
            },
            {
                id: 4,
                name: 'Flash',
                owner: 'DC'
            },
        ];

        expect(heroes).toEqual(heroesDc)        
        
    })

    test(' getHeroesByOwner debe retornar los heroes de Marvel ',()=>{
        const owner="Marvel"
        const heroes= getHeroesByOwner(owner);
        expect(heroes.length).toBe(2)        
    })

})
