const { getHeroeByIdAsync } = require("../../base/09-promesas");
import {heroes} from '../../data/heroes'
describe('Pruebas con promesas',()=>{

    test(' Debe retornar un heroe Asyncronó ',(done)=>{

        //Inicialización
        const id =1;
        const hero= getHeroeByIdAsync(id).then((hero)=>{
            expect(hero).toBe(heroes[0])
            done()
        });

    })

    test(' Debe retornar un error de heroe inexistente ',(done)=>{

        //Inicialización
        const id =10;
        const hero= getHeroeByIdAsync(id).catch((err)=>{
            expect(err).toBe('No se pudo encontrar el héroe')
            done()
        });

    })

})
