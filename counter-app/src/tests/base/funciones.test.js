const { getUser, getUsuarioActivo } = require("../../base/05-funciones");

describe('Pruebas en el archivo funciones',()=>{

    test(' getUser debe retornar un objeto ',()=>{
        //Inicialización
        const usuario={
            uid: 'ABC123',
            username: 'El_Papi1502'
    };
    const user =getUser();


        expect(user).toEqual(usuario)
    })

    test(' getUserActivo debe retornar un objeto con el nombre especificado ',()=>{
        //Inicialización
        const nombre='Karen';
        const usuario=    {
            uid: 'ABC567',
            username: nombre
        };

    const user =getUsuarioActivo(nombre);


        expect(user).toEqual(usuario)
    })


})