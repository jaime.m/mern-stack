const { getSaludo } = require("../../base/02-template-string");

describe('Pruebas en el archivo 02 Template String',()=>{

    test(' getSaludo debe retornar hola karen ',()=>{
        //Inicialización
        const nombre='karen';
        //Estimulo
        const saludo= getSaludo(nombre)
        const mensaje2=`false`
        //observar el comportamiento
        expect(saludo).toBe('Hola karen')
    })

    test(' getSaludo debe retornar hola jaime si no hay argumentos ',()=>{
        //Inicialización
        //Estimulo
        const saludo= getSaludo()
        const mensaje2=`false`
        //observar el comportamiento
        expect(saludo).toBe('Hola Jaime')
    })
    
})