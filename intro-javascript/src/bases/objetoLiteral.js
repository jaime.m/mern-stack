const p={
    name:'name',
    lname:'lnname',
    age:34,
    add:{
        city:'ny',
        zip:134,
        lat:234234,
        long:23123
    }
}

const p2={...p}
p2.name='p2'

console.table({p});
console.table({p2});
